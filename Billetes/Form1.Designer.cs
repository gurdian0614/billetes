﻿namespace Billetes
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPtos = new System.Windows.Forms.TextBox();
            this.txtConfirmar = new System.Windows.Forms.Button();
            this.txtLimpiar = new System.Windows.Forms.Button();
            this.txtSalir = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnConectar = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btn02 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.btn01 = new System.Windows.Forms.Button();
            this.txt500 = new System.Windows.Forms.Button();
            this.txt100 = new System.Windows.Forms.Button();
            this.txt50 = new System.Windows.Forms.Button();
            this.txt20 = new System.Windows.Forms.Button();
            this.txt10 = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPtos
            // 
            this.txtPtos.BackColor = System.Drawing.Color.Black;
            this.txtPtos.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPtos.ForeColor = System.Drawing.Color.Lime;
            this.txtPtos.Location = new System.Drawing.Point(1, 1);
            this.txtPtos.Multiline = true;
            this.txtPtos.Name = "txtPtos";
            this.txtPtos.Size = new System.Drawing.Size(731, 103);
            this.txtPtos.TabIndex = 8;
            this.txtPtos.Text = "0 Puntos";
            // 
            // txtConfirmar
            // 
            this.txtConfirmar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txtConfirmar.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirmar.Location = new System.Drawing.Point(414, 490);
            this.txtConfirmar.Name = "txtConfirmar";
            this.txtConfirmar.Size = new System.Drawing.Size(199, 65);
            this.txtConfirmar.TabIndex = 9;
            this.txtConfirmar.Text = "CONFIRMAR";
            this.txtConfirmar.UseVisualStyleBackColor = true;
            this.txtConfirmar.Click += new System.EventHandler(this.txtConfirmar_Click);
            // 
            // txtLimpiar
            // 
            this.txtLimpiar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txtLimpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLimpiar.Location = new System.Drawing.Point(619, 490);
            this.txtLimpiar.Name = "txtLimpiar";
            this.txtLimpiar.Size = new System.Drawing.Size(199, 65);
            this.txtLimpiar.TabIndex = 10;
            this.txtLimpiar.Text = "LIMPIAR";
            this.txtLimpiar.UseVisualStyleBackColor = true;
            this.txtLimpiar.Click += new System.EventHandler(this.txtLimpiar_Click);
            // 
            // txtSalir
            // 
            this.txtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txtSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalir.Location = new System.Drawing.Point(824, 490);
            this.txtSalir.Name = "txtSalir";
            this.txtSalir.Size = new System.Drawing.Size(199, 65);
            this.txtSalir.TabIndex = 11;
            this.txtSalir.Text = "SALIR";
            this.txtSalir.UseVisualStyleBackColor = true;
            this.txtSalir.Click += new System.EventHandler(this.txtSalir_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.btnConectar);
            this.groupBox2.Location = new System.Drawing.Point(17, 499);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(375, 51);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Serial Connection";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(6, 17);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(196, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // btnConectar
            // 
            this.btnConectar.Location = new System.Drawing.Point(206, 13);
            this.btnConectar.Margin = new System.Windows.Forms.Padding(2);
            this.btnConectar.Name = "btnConectar";
            this.btnConectar.Size = new System.Drawing.Size(165, 28);
            this.btnConectar.TabIndex = 0;
            this.btnConectar.Text = "CONECTAR";
            this.btnConectar.UseVisualStyleBackColor = true;
            this.btnConectar.Click += new System.EventHandler(this.btnConectar_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Black;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.Lime;
            this.textBox1.Location = new System.Drawing.Point(733, 1);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(295, 103);
            this.textBox1.TabIndex = 14;
            this.textBox1.Text = "MQ 00";
            // 
            // btn02
            // 
            this.btn02.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.btn02.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn02.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn02.Location = new System.Drawing.Point(209, 198);
            this.btn02.Name = "btn02";
            this.btn02.Size = new System.Drawing.Size(199, 65);
            this.btn02.TabIndex = 16;
            this.btn02.Text = "02";
            this.btn02.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn02.UseVisualStyleBackColor = true;
            this.btn02.Click += new System.EventHandler(this.btn02_Click);
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(414, 198);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(199, 65);
            this.button3.TabIndex = 17;
            this.button3.Text = "03";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(619, 198);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(199, 65);
            this.button4.TabIndex = 18;
            this.button4.Text = "04";
            this.button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(824, 198);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(199, 65);
            this.button5.TabIndex = 19;
            this.button5.Text = "05";
            this.button5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(4, 268);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(199, 65);
            this.button6.TabIndex = 20;
            this.button6.Text = "06";
            this.button6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(209, 268);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(199, 65);
            this.button7.TabIndex = 21;
            this.button7.Text = "07";
            this.button7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(414, 268);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(199, 65);
            this.button8.TabIndex = 22;
            this.button8.Text = "08";
            this.button8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(619, 268);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(199, 65);
            this.button9.TabIndex = 23;
            this.button9.Text = "09";
            this.button9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(414, 410);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(199, 65);
            this.button10.TabIndex = 32;
            this.button10.Text = "18";
            this.button10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(209, 410);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(199, 65);
            this.button11.TabIndex = 31;
            this.button11.Text = "17";
            this.button11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(4, 410);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(199, 65);
            this.button12.TabIndex = 30;
            this.button12.Text = "16";
            this.button12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(824, 339);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(199, 65);
            this.button13.TabIndex = 29;
            this.button13.Text = "15";
            this.button13.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(619, 339);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(199, 65);
            this.button14.TabIndex = 28;
            this.button14.Text = "14";
            this.button14.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(414, 339);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(199, 65);
            this.button15.TabIndex = 27;
            this.button15.Text = "13";
            this.button15.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Location = new System.Drawing.Point(209, 339);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(199, 65);
            this.button16.TabIndex = 26;
            this.button16.Text = "12";
            this.button16.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(4, 339);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(199, 65);
            this.button17.TabIndex = 25;
            this.button17.Text = "11";
            this.button17.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.Location = new System.Drawing.Point(824, 268);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(199, 65);
            this.button18.TabIndex = 24;
            this.button18.Text = "10";
            this.button18.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.Location = new System.Drawing.Point(824, 410);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(199, 65);
            this.button19.TabIndex = 34;
            this.button19.Text = "20";
            this.button19.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.button20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Location = new System.Drawing.Point(619, 410);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(199, 65);
            this.button20.TabIndex = 33;
            this.button20.Text = "19";
            this.button20.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button20.UseVisualStyleBackColor = true;
            // 
            // btn01
            // 
            this.btn01.BackgroundImage = global::Billetes.Properties.Resources.IMG_20190813_WA0046;
            this.btn01.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn01.Location = new System.Drawing.Point(4, 198);
            this.btn01.Name = "btn01";
            this.btn01.Size = new System.Drawing.Size(199, 65);
            this.btn01.TabIndex = 15;
            this.btn01.Text = "01";
            this.btn01.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn01.UseVisualStyleBackColor = true;
            this.btn01.Click += new System.EventHandler(this.btn01_Click);
            // 
            // txt500
            // 
            this.txt500.BackgroundImage = global::Billetes.Properties.Resources._500lempiras;
            this.txt500.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txt500.Enabled = false;
            this.txt500.Location = new System.Drawing.Point(824, 111);
            this.txt500.Name = "txt500";
            this.txt500.Size = new System.Drawing.Size(199, 82);
            this.txt500.TabIndex = 7;
            this.txt500.UseVisualStyleBackColor = true;
            this.txt500.Click += new System.EventHandler(this.txt500_Click);
            // 
            // txt100
            // 
            this.txt100.BackgroundImage = global::Billetes.Properties.Resources._100lempira;
            this.txt100.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txt100.Enabled = false;
            this.txt100.Location = new System.Drawing.Point(619, 111);
            this.txt100.Name = "txt100";
            this.txt100.Size = new System.Drawing.Size(199, 82);
            this.txt100.TabIndex = 6;
            this.txt100.UseVisualStyleBackColor = true;
            this.txt100.Click += new System.EventHandler(this.txt100_Click);
            // 
            // txt50
            // 
            this.txt50.BackgroundImage = global::Billetes.Properties.Resources._50lempiras;
            this.txt50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txt50.Enabled = false;
            this.txt50.Location = new System.Drawing.Point(414, 111);
            this.txt50.Name = "txt50";
            this.txt50.Size = new System.Drawing.Size(199, 82);
            this.txt50.TabIndex = 5;
            this.txt50.UseVisualStyleBackColor = true;
            this.txt50.Click += new System.EventHandler(this.txt50_Click);
            // 
            // txt20
            // 
            this.txt20.BackgroundImage = global::Billetes.Properties.Resources._20lempiras;
            this.txt20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txt20.Enabled = false;
            this.txt20.Location = new System.Drawing.Point(209, 111);
            this.txt20.Name = "txt20";
            this.txt20.Size = new System.Drawing.Size(199, 82);
            this.txt20.TabIndex = 4;
            this.txt20.UseVisualStyleBackColor = true;
            this.txt20.Click += new System.EventHandler(this.txt20_Click);
            // 
            // txt10
            // 
            this.txt10.BackgroundImage = global::Billetes.Properties.Resources._10lempiras;
            this.txt10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.txt10.Enabled = false;
            this.txt10.Location = new System.Drawing.Point(4, 111);
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(199, 82);
            this.txt10.TabIndex = 3;
            this.txt10.UseVisualStyleBackColor = true;
            this.txt10.Click += new System.EventHandler(this.txt10_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 565);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn02);
            this.Controls.Add(this.btn01);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtSalir);
            this.Controls.Add(this.txtLimpiar);
            this.Controls.Add(this.txtConfirmar);
            this.Controls.Add(this.txtPtos);
            this.Controls.Add(this.txt500);
            this.Controls.Add(this.txt100);
            this.Controls.Add(this.txt50);
            this.Controls.Add(this.txt20);
            this.Controls.Add(this.txt10);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Puntaje";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button txt10;
        private System.Windows.Forms.Button txt500;
        private System.Windows.Forms.Button txt100;
        private System.Windows.Forms.Button txt50;
        private System.Windows.Forms.Button txt20;
        private System.Windows.Forms.TextBox txtPtos;
        private System.Windows.Forms.Button txtConfirmar;
        private System.Windows.Forms.Button txtLimpiar;
        private System.Windows.Forms.Button txtSalir;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnConectar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btn01;
        private System.Windows.Forms.Button btn02;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
    }
}

