﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace Billetes
{
    public partial class Form1 : Form
    {
        bool isConnected = false;
        String[] ports;
        SerialPort port;
        public Form1()
        {
            InitializeComponent();
            getAvailableComPorts();

            foreach (string port in ports)
            {
                comboBox1.Items.Add(port);
                if (ports[0] != null)
                {
                    comboBox1.SelectedItem = -1;
                }
            }
        }

        double ptos = 0;

        private void calcularPtos(int cant)
        {
            ptos += cant;

            if (ptos == 1)
            {
                txtPtos.Text = ptos.ToString() + " Punto";
            }
            else
            {
                txtPtos.Text = ptos.ToString() + " Puntos";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ptos = 0;
        }

        private void txt1_Click(object sender, EventArgs e)
        {
            calcularPtos(1);
        }

        private void txt2_Click(object sender, EventArgs e)
        {
            calcularPtos(2);
        }

        private void txt5_Click(object sender, EventArgs e)
        {
            calcularPtos(5);
        }

        private void txt10_Click(object sender, EventArgs e)
        {
            calcularPtos(10);
        }

        private void txt20_Click(object sender, EventArgs e)
        {
            calcularPtos(20);
        }

        private void txt50_Click(object sender, EventArgs e)
        {
            calcularPtos(50);
        }

        private void txt100_Click(object sender, EventArgs e)
        {
            calcularPtos(100);
        }

        private void txt500_Click(object sender, EventArgs e)
        {
            calcularPtos(500);
        }

        private void txtConfirmar_Click(object sender, EventArgs e)
        {
            if (isConnected)
            {
                port.Write("#" + ptos.ToString() + "#\n");
            }
        }

        private void txtLimpiar_Click(object sender, EventArgs e)
        {
            ptos = 0;
            txtPtos.Text = "0 Puntos";
        }

        private void txtSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            if (!isConnected)
            {
                connectToArduino();
            }
            else
            {
                disconnectFromArduino();
            }
        }

        void getAvailableComPorts()
        {
            ports = SerialPort.GetPortNames();
        }

        private void connectToArduino()
        {
            try
            {
                isConnected = true;
                string selectedPort = comboBox1.GetItemText(comboBox1.SelectedItem);
                port = new SerialPort(selectedPort, 9600, Parity.None, 8, StopBits.One);
                port.Open();
                //port.Write("START\n");
                btnConectar.Text = "DESCONECTAR";
                //string lectura = port.ReadLine();
                //MessageBox.Show(lectura);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void disconnectFromArduino()
        {
            isConnected = false;
            //port.Write("#STOP\n");
            port.Close();
            btnConectar.Text = "CONECTAR";
        }

        private void btn01_Click(object sender, EventArgs e)
        {
            if (isConnected)
            {
                port.Write("1\n");
                if (port.ReadLine() == "OK\r")
                {
                    MessageBox.Show("Excelente");
                }
            }
        }

        private void btn02_Click(object sender, EventArgs e)
        {
            if (isConnected)
            {
                port.Write("0\n");
            }
        }
    }
}
